# -*- coding: utf-8 -*-
import wx

__author__ = 'ozgur'
__creation_date__ = '11/29/15' '7:26 PM'
from mainwindow_gui import MainForm


if __name__ == '__main__':

    app = wx.App(redirect=False)   # Error messages go to popup window
    main_frame = MainForm(None)
    main_frame.Show()

    app.MainLoop()