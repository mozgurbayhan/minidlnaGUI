# -*- coding: utf-8 -*-
import signal

__author__ = 'ozgur'
__creation_date__ = '12/8/15' '11:10 PM'
import os

USER_HOME_PATH = os.environ['HOME']
APP_HOME_PATH = os.path.join(USER_HOME_PATH, ".minidlnaGUI")

MINIDLNA_CONF_PATH = os.path.join(APP_HOME_PATH, "minidlna.conf")
MINIDLNA_GUI_CONF_PATH = os.path.join(APP_HOME_PATH, "minidlnagui.conf")
MINIDLNA_LOG_PATH = os.path.join(APP_HOME_PATH, "minidlna.log")
MINIDLNA_DB_PATH = os.path.join(APP_HOME_PATH, "files.db")
MINIDLNA_SOCK_PATH = os.path.join(APP_HOME_PATH, "minissdpd.sock")
MINIDLNA_PID_PATH = os.path.join(APP_HOME_PATH, "minissdpd.pid")


# MINIDLNA_PATH = ""


class MiniLog():
    def __init__(self):
        pass

    # noinspection PyMethodMayBeStatic
    def read_minidlna_log(self):
        """
        opens and reads minidlna log file\n
        :return: None
        """
        ret_list = []
        if not os.path.exists(MINIDLNA_LOG_PATH):
            f = open(MINIDLNA_LOG_PATH, "w")
            f.write("")
            f.close()

        with open(MINIDLNA_LOG_PATH, "r") as myfile:
            lines = myfile.read().split("\n")

            for line in reversed(lines):
                if len(line) > 1:
                    try:
                        s_line = line.split("]")
                        d_part = s_line[0][1:]
                        log_part = "".join(s_line[1:])
                        _date, _time = d_part.split(" ")
                        log_parts = log_part.split(":")
                        _module = log_parts[0].strip()
                        _log_line = log_parts[1].strip()
                        _level = log_parts[2].strip()
                        _log = ("".join(log_parts[3:])).strip()
                        ret_list.append(
                            [_date.strip(), _time.strip(), _module, _log_line, _log, _level])
                    except Exception as ex:
                        ret_list.append(["", "", "", "", line, ""])

                        return ret_list


class MiniProcess():
    def __init__(self, config):
        """
        process controller for minidlna

        :param config: Instance of  MinidlnaConfig

        :return:
        """
        if isinstance(config, MinidlnaConfig):
            self.minidlnapath = config.g_config_dict["minidlnapath"]
        else:
            raise TypeError

    # noinspection PyMethodMayBeStatic
    def is_minidlna_running(self):
        """
        checks if minidlna running \n
        :return: None
        """
        retval = False
        processname = self.minidlnapath
        for line in os.popen("ps xa"):
            fields = line.split()
            # pid = fields[0]
            process = fields[4]
            if process == processname:
                retval = True
        return retval

    def start_minidlna(self):
        """
        starts minidlna\n
        :return:  None
        """
        retval = True
        if not self.is_minidlna_running():
            # command = self.minidlnapath + " -f " + MINIDLNA_CONF_PATH
            command = self.minidlnapath + " -f " + MINIDLNA_CONF_PATH + " -P " + MINIDLNA_PID_PATH
            os.system(command)
        else:
            retval = False
        return retval

    def stop_minidlna(self):
        """
        stops minidlna \n
        :return:  None
        """
        retval = True
        processname = self.minidlnapath
        for line in os.popen("ps xa"):
            fields = line.split()
            pid = fields[0]
            process = fields[4]
            if process == processname:
                try:
                    os.kill(int(pid), signal.SIGKILL)
                except OSError:
                    retval = False
        return retval

    def rebuild_minidlna_db(self):
        """
        triggers db rebuild function of minidlna \n
        :return: None
        """
        # Didn't use minidlna -R because sometimes doesnt work
        self.stop_minidlna()
        os.remove(MINIDLNA_DB_PATH)
        self.start_minidlna()


class MinidlnaConfig():
    def __init__(self):
        """
        configuration controller for minidlna

        :return:
        """

        self.minidir = APP_HOME_PATH
        if not os.path.exists(self.minidir):
            os.mkdir(self.minidir)

        # # GUI OPTIONS
        self.start_minidlna_with_gui = False
        self.stop_minidlna_with_gui = False

        self.config_dict = {
            "media_dirs": [],
            "log_dir": APP_HOME_PATH,
            "db_dir": APP_HOME_PATH,
            "log_level": "general,artwork,database,inotify,scanner,metadata,http,ssdp",
            "album_art_names": "Cover.jpg/cover.jpg/AlbumArtSmall.jpg/albumartsmall.jpg/AlbumArt.jpg/albumart.jpg/Album.jpg/album.jpg/Folder.jpg/folder.jpg/Thumb.jpg/thumb.jpg",
            "force_sort_criteria": "+upnp:class,+upnp:originalTrackNumber,+dc:title",
            "port": 8200,
            "merge_media_dirs": "no",
            "friendly_name": "minidlnaGUI",
            "inotify": "yes",
            "enable_tivo": "no",
            "strict_dlna": "no",
            "notify_interval": 900,
            "serial": "123456789",
            "model_number": 1,
            "minissdpdsocket": MINIDLNA_SOCK_PATH,
            "root_container": ".",
            "max_connections": 50

        }

        self.g_config_dict = {
            "startwithgui": "no",
            "stopwithgui": "no",
            "minidlnapath": "/usr/bin/minidlnad"
        }

    # noinspection PyMethodMayBeStatic
    def safe_cast(self, val, to_type, default=None):
        try:
            return to_type(val)
        except ValueError:
            return default

    def read_config(self):
        """
        reads config from file
        :return:
        """
        # Read Minidlna GUI Config
        if os.path.isfile(MINIDLNA_GUI_CONF_PATH):
            with open(MINIDLNA_GUI_CONF_PATH) as f:
                lines = f.readlines()
                for line in lines:
                    line = line.strip()
                    if line is not "" and line[0] is not "#":
                        pair = line.split("=")
                        self.g_config_dict[pair[0].strip()] = pair[1].strip()
                    else:
                        pass
        else:
            pass
            # print "Can not read config file : ", MINIDLNA_GUI_CONF_PATH

        # Read Minidlna Config
        if os.path.isfile(MINIDLNA_CONF_PATH):
            with open(MINIDLNA_CONF_PATH) as f:
                lines = f.readlines()
                for line in lines:
                    line = line.strip()
                    if line is not "" and line[0] is not "#":
                        pair = line.split("=")
                        if pair[0].strip() == "media_dir":
                            self.config_dict["media_dirs"].append(pair[1].strip())
                        else:
                            self.config_dict[pair[0].strip()] = pair[1].strip()
        else:
            pass
            # print "Can not read config file : ", MINIDLNA_CONF_PATH

    def write_config(self):
        """
        writes config to file
        :return:
        """

        # Read Minidlna GUI Config
        config_text = ""
        for key, value in self.config_dict.iteritems():
            if key is "media_dirs":
                for md in value:
                    config_text += "media_dir=" + md + "\n"
            elif value == "" or value == 0:
                pass
            else:
                config_text += key + "=" + str(value) + "\n"
        f = open(MINIDLNA_CONF_PATH, 'w+')
        f.write(config_text)
        f.close()

        # Write Minidlna GUI Config
        config_text = ""
        for key, value in self.g_config_dict.iteritems():
            if value == "" or value == 0:
                pass
            else:
                config_text += key + "=" + str(value) + "\n"
        f = open(MINIDLNA_GUI_CONF_PATH, 'w+')
        f.write(config_text)
        f.close()
