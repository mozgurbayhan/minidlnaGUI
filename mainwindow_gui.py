# -*- coding: utf-8 -*-
import os
# noinspection PyPackageRequirements
import wx
# noinspection PyPackageRequirements
import wx.xrc

from mainwindow_gui_design import MainFormDesign
from backend_lib import MiniProcess, MinidlnaConfig, MiniLog
from images import GetIcon_icon_ico

GREEN = (41, 73, 10)
RED = (117, 3, 16)
YELLOW = (210, 109, 28)


class MainForm(MainFormDesign):
    def __init__(self, parent):
        super(MainForm, self).__init__(parent)

        favicon=GetIcon_icon_ico()
        wx.Frame.SetIcon(self, favicon)

        self._set_netcards_combo()
        self.mc = MinidlnaConfig()
        self.ml = MiniLog()
        self.mc.read_config()
        self._load_config()
        self.mp = MiniProcess(self.mc)
        if str(self.mc.g_config_dict["startwithgui"]) == "yes":
            self.mp.stop_minidlna()
            self.mp.start_minidlna()

        self.lst_ctrl_log.InsertColumn(0, "Date", width=75)
        self.lst_ctrl_log.InsertColumn(1, "Time", width=60)
        self.lst_ctrl_log.InsertColumn(2, "Module", width=75)
        self.lst_ctrl_log.InsertColumn(3, "Line", width=40)
        self.lst_ctrl_log.InsertColumn(4, "Log", width=510)

        self.mrunning = False

    def _set_netcards_combo(self):
        """
        fills network card combo box \n
        :return:
        """
        netcards = os.listdir('/sys/class/net/')
        for netcard in netcards:
            self.cmb_network_interface.Append(netcard)

    def _save_config(self):
        """
        writes minidlna config\n
        :return:
        """
        config_dict = self.mc.config_dict

        config_dict["merge_media_dirs"] = self.cmb_merge_media_dirs.GetValue()
        config_dict["inotify"] = self.cmb_inotify.GetValue()
        config_dict["notify_interval"] = self.num_notify_interval.GetValue()
        config_dict["enable_tivo"] = self.cmb_enable_tivo.GetValue()
        config_dict["strict_dlna"] = self.cmb_strict_dlna.GetValue()
        config_dict["root_container"] = self.cmb_root_container.GetValue()
        config_dict["friendly_name"] = self.txt_friendly_name.GetValue()
        config_dict["presentation_url"] = self.txt_presentation_url.GetValue()
        config_dict["serial"] = self.txt_serial.GetValue()
        config_dict["model_number"] = self.num_model_number.GetValue()
        config_dict["network_interface"] = self.cmb_network_interface.GetValue()
        config_dict["port"] = self.num_port.GetValue()
        config_dict["max_connections"] = self.num_max_connections.GetValue()

        media_dirs = []
        for item in self.lst_paths.Items:
            media_dirs.append(item)
        config_dict["media_dirs"] = media_dirs
        self.mc.config_dict = config_dict

        # Write Minidlna GUI Config

        self.mc.g_config_dict["startwithgui"] = self.cmb_start_minidlna_with_gui.GetValue()
        self.mc.g_config_dict["stopwithgui"] = self.cmb_stop_minidlna_with_gui.GetValue()
        self.mc.g_config_dict["minidlnapath"] = self.txt_minidlna_path.GetValue()

        self.mc.write_config()

    def _load_config(self):
        """
        reads minidlna config \n
        :return:
        """
        config_dict = self.mc.config_dict

        self.cmb_merge_media_dirs.SetValue(config_dict["merge_media_dirs"])
        self.cmb_inotify.SetValue(config_dict["inotify"])
        self.num_notify_interval.SetValue(self.mc.safe_cast(config_dict["notify_interval"], int, 895))
        self.cmb_enable_tivo.SetValue(config_dict["enable_tivo"])
        self.cmb_strict_dlna.SetValue(config_dict["strict_dlna"])
        self.cmb_root_container.SetValue(config_dict["root_container"])
        self.txt_friendly_name.SetValue(config_dict["friendly_name"])
        self.txt_serial.SetValue(config_dict["serial"])
        self.num_model_number.SetValue(self.mc.safe_cast(config_dict["model_number"], int, 1))
        self.num_port.SetValue(self.mc.safe_cast(config_dict["port"], int, 0))
        self.num_max_connections.SetValue(self.mc.safe_cast(config_dict["max_connections"], int, 50))
        if "network_interface" in config_dict:
            self.cmb_network_interface.SetValue(config_dict["network_interface"])
        if "presentation_url" in config_dict:
            self.txt_presentation_url.SetValue(config_dict["presentation_url"])
        if "media_dirs" in config_dict:
            for path in config_dict["media_dirs"]:
                self.lst_paths.Clear()
                self.lst_paths.Append(path)

        # Read Minidlna GUI Config
        self.cmb_start_minidlna_with_gui.SetValue(self.mc.g_config_dict["startwithgui"])
        self.cmb_stop_minidlna_with_gui.SetValue(self.mc.g_config_dict["stopwithgui"])
        self.txt_minidlna_path.SetValue(self.mc.g_config_dict["minidlnapath"])

    def _fill_log_list(self):
        """
        fills log list \n
        :return:
        """
        self.lst_ctrl_log.DeleteAllItems()
        log_list = self.ml.read_minidlna_log()
        index = 0
        for line in log_list:
            _date, _time, _module, _log_line, _log, _level = line
            if _level == "error":
                color = RED
            elif _level == "warn":
                color = YELLOW
            else:
                color = GREEN

            item = wx.ListItem()
            item.SetText(_date)
            item.SetTextColour(color)
            item.SetId(index)
            self.lst_ctrl_log.InsertItem(item)
            self.lst_ctrl_log.SetStringItem(index, 1, _time)
            self.lst_ctrl_log.SetStringItem(index, 2, _module)
            self.lst_ctrl_log.SetStringItem(index, 3, _log_line)
            self.lst_ctrl_log.SetStringItem(index, 4, _log)

            index += 1

    ######## EVENTS ########
    def event_timer_check(self, event):
        self.mrunning = self.mp.is_minidlna_running()
        if self.mrunning:
            self.lbl_status.SetLabel("miniDLNA is running")
            self.lbl_status.SetForegroundColour(GREEN)
        else:
            self.lbl_status.SetLabel("miniDLNA is not running")
            self.lbl_status.SetForegroundColour(RED)

    def event_btn_Rebuild_clicked(self, event):
        self.mp.rebuild_minidlna_db()

    def event_btn_Start_clicked(self, event):
        self.mp.start_minidlna()

    def event_btn_Stop_clicked(self, event):
        self.mp.stop_minidlna()

    def event_btn_add_path_click(self, event):
        if self.txt_serve_folder_select.GetValue() != "":
            prefix = self.cmb_media_dir_type.GetValue()
            if prefix == "All":
                prefix = ""
            else:
                prefix += ","
            line = self.txt_serve_folder_select.GetValue()
            self.lst_paths.Append(prefix + line)
            self.lbl_wxfp_warn.SetLabel("")
            self.txt_serve_folder_select.SetValue("")
        else:
            self.lbl_wxfp_warn.SetLabel("Please select a path to add first!")

    def event_btn_serve_folder_select_click(self, event):
        openfiledialog = wx.DirDialog(None, "Please Select Media Path To Add", "~/", 0, (10, 10), wx.Size(400, 300))
        openfiledialog.CentreOnParent()

        if openfiledialog.ShowModal() == wx.ID_CANCEL:
            pass
        else:
            self.txt_serve_folder_select.SetValue(openfiledialog.GetPath())

    def event_btn_remove_path_click(self, event):
        self.lst_paths.Delete(self.lst_paths.GetSelection())

    def event_btn_save_config_clicked(self, event):
        self._save_config()

    def event_form_close(self, event):
        if str(self.mc.g_config_dict["stopwithgui"]) == "yes":
            self.mp.stop_minidlna()
        self.Destroy()

    def event_tab_cont_main_changed(self, event):
        if self.tab_cont_main.GetSelection() == 2:
            self._fill_log_list()

    def event_btn_minidlna_path_select_click(self, event):
        openfiledialog = wx.FileDialog(self, "Select minidlna path", "", self.txt_minidlna_path.GetValue(), "", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)

        if openfiledialog.ShowModal() == wx.ID_CANCEL:
            pass
        else:
            self.txt_minidlna_path.SetValue(openfiledialog.GetPath())