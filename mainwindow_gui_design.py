# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Apr 19 2016)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

import gettext
_ = gettext.gettext

###########################################################################
## Class MainFormDesign
###########################################################################

class MainFormDesign ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = _(u"Minidlna GUI"), pos = wx.DefaultPosition, size = wx.Size( 800,600 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer1 = wx.BoxSizer( wx.VERTICAL )
		
		self.tab_cont_main = wx.Notebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.NB_FIXEDWIDTH )
		self.tab_base = wx.Panel( self.tab_cont_main, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		self.btn_Start = wx.Button( self.tab_base, wx.ID_ANY, _(u"Start"), wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )
		bSizer2.Add( self.btn_Start, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.btn_Stop = wx.Button( self.tab_base, wx.ID_ANY, _(u"Stop"), wx.DefaultPosition, wx.DefaultSize, wx.BU_EXACTFIT )
		bSizer2.Add( self.btn_Stop, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.btn_Rebuild = wx.Button( self.tab_base, wx.ID_ANY, _(u"Rebuild DB"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.btn_Rebuild.SetToolTipString( _(u"Stops dlna, removes db, rebuilds db, and  starts again") )
		
		bSizer2.Add( self.btn_Rebuild, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.tab_base.SetSizer( bSizer2 )
		self.tab_base.Layout()
		bSizer2.Fit( self.tab_base )
		self.tab_cont_main.AddPage( self.tab_base, _(u"miniDLNA"), True )
		self.tab_config = wx.Panel( self.tab_cont_main, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_notebook3 = wx.Notebook( self.tab_config, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.NB_LEFT )
		self.pnl_config_gui = wx.Panel( self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.pnl_config_gui.SetToolTipString( _(u"Start dlna when application starts.") )
		
		bSizer41 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText171 = wx.StaticText( self.pnl_config_gui, wx.ID_ANY, _(u"Start DLNA with GUI : "), wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.m_staticText171.Wrap( -1 )
		bSizer10.Add( self.m_staticText171, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		cmb_start_minidlna_with_guiChoices = [ _(u"yes"), _(u"no") ]
		self.cmb_start_minidlna_with_gui = wx.ComboBox( self.pnl_config_gui, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_start_minidlna_with_guiChoices, wx.CB_READONLY )
		self.cmb_start_minidlna_with_gui.SetToolTipString( _(u"Start miniDLNA when application started.") )
		
		bSizer10.Add( self.cmb_start_minidlna_with_gui, 0, wx.ALL, 5 )
		
		
		bSizer41.Add( bSizer10, 0, wx.EXPAND, 5 )
		
		bSizer11 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText18 = wx.StaticText( self.pnl_config_gui, wx.ID_ANY, _(u"Stop DLNA with GUI : "), wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.m_staticText18.Wrap( -1 )
		bSizer11.Add( self.m_staticText18, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		cmb_stop_minidlna_with_guiChoices = [ _(u"yes"), _(u"no") ]
		self.cmb_stop_minidlna_with_gui = wx.ComboBox( self.pnl_config_gui, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_stop_minidlna_with_guiChoices, wx.CB_READONLY )
		self.cmb_stop_minidlna_with_gui.SetToolTipString( _(u"Stop miniDLNA when application closed.") )
		
		bSizer11.Add( self.cmb_stop_minidlna_with_gui, 0, wx.ALL, 5 )
		
		bSizer14 = wx.BoxSizer( wx.VERTICAL )
		
		
		bSizer11.Add( bSizer14, 1, wx.EXPAND, 5 )
		
		
		bSizer41.Add( bSizer11, 0, wx.EXPAND, 5 )
		
		bSizer15 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText21 = wx.StaticText( self.pnl_config_gui, wx.ID_ANY, _(u"minidlna Path"), wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		self.m_staticText21.Wrap( -1 )
		bSizer15.Add( self.m_staticText21, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.txt_minidlna_path = wx.TextCtrl( self.pnl_config_gui, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 500,-1 ), 0 )
		bSizer15.Add( self.txt_minidlna_path, 0, wx.ALL, 5 )
		
		self.btn_minidlna_path_select = wx.Button( self.pnl_config_gui, wx.ID_ANY, _(u"..."), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer15.Add( self.btn_minidlna_path_select, 0, wx.ALL, 5 )
		
		
		bSizer41.Add( bSizer15, 0, wx.EXPAND, 5 )
		
		
		self.pnl_config_gui.SetSizer( bSizer41 )
		self.pnl_config_gui.Layout()
		bSizer41.Fit( self.pnl_config_gui )
		self.m_notebook3.AddPage( self.pnl_config_gui, _(u"GUI"), True )
		self.pnl_config_general = wx.Panel( self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer8 = wx.BoxSizer( wx.HORIZONTAL )
		
		fgSizer31 = wx.FlexGridSizer( 5, 2, 0, 0 )
		fgSizer31.SetFlexibleDirection( wx.BOTH )
		fgSizer31.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText11 = wx.StaticText( self.pnl_config_general, wx.ID_ANY, _(u"Merge Media Dirs"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText11.Wrap( -1 )
		fgSizer31.Add( self.m_staticText11, 0, wx.ALL, 5 )
		
		cmb_merge_media_dirsChoices = [ _(u"yes"), _(u"no"), wx.EmptyString ]
		self.cmb_merge_media_dirs = wx.ComboBox( self.pnl_config_general, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_merge_media_dirsChoices, 0 )
		self.cmb_merge_media_dirs.SetToolTipString( _(u"Set this to merge all media_dir base contents into the root container") )
		
		fgSizer31.Add( self.cmb_merge_media_dirs, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText13 = wx.StaticText( self.pnl_config_general, wx.ID_ANY, _(u"Bind Inotify"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText13.Wrap( -1 )
		fgSizer31.Add( self.m_staticText13, 0, wx.ALL, 5 )
		
		cmb_inotifyChoices = [ _(u"yes"), _(u"no"), wx.EmptyString ]
		self.cmb_inotify = wx.ComboBox( self.pnl_config_general, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_inotifyChoices, 0 )
		self.cmb_inotify.SetToolTipString( _(u"Set this to no to disable inotify monitoring to automatically discover new files") )
		
		fgSizer31.Add( self.cmb_inotify, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText16 = wx.StaticText( self.pnl_config_general, wx.ID_ANY, _(u"Notify Interval"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText16.Wrap( -1 )
		fgSizer31.Add( self.m_staticText16, 0, wx.ALL, 5 )
		
		self.num_notify_interval = wx.SpinCtrl( self.pnl_config_general, wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10000000, 0 )
		self.num_notify_interval.SetToolTipString( _(u"Notify interval in seconds. Default is 895 seconds.") )
		
		fgSizer31.Add( self.num_notify_interval, 0, wx.ALL, 5 )
		
		
		bSizer8.Add( fgSizer31, 1, wx.EXPAND, 5 )
		
		self.m_staticline3 = wx.StaticLine( self.pnl_config_general, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		bSizer8.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )
		
		fgSizer4 = wx.FlexGridSizer( 5, 2, 0, 0 )
		fgSizer4.SetFlexibleDirection( wx.BOTH )
		fgSizer4.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText14 = wx.StaticText( self.pnl_config_general, wx.ID_ANY, _(u"Enable Tivo"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText14.Wrap( -1 )
		fgSizer4.Add( self.m_staticText14, 0, wx.ALL, 5 )
		
		cmb_enable_tivoChoices = [ _(u"yes"), _(u"no"), wx.EmptyString ]
		self.cmb_enable_tivo = wx.ComboBox( self.pnl_config_general, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_enable_tivoChoices, 0 )
		self.cmb_enable_tivo.SetToolTipString( _(u"Set this to yes to enable support for streaming .jpg and .mp3 files to a TiVo supporting HMO") )
		
		fgSizer4.Add( self.cmb_enable_tivo, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText15 = wx.StaticText( self.pnl_config_general, wx.ID_ANY, _(u"Strict DLNA"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText15.Wrap( -1 )
		fgSizer4.Add( self.m_staticText15, 0, wx.ALL, 5 )
		
		cmb_strict_dlnaChoices = [ _(u"yes"), _(u"no"), wx.EmptyString ]
		self.cmb_strict_dlna = wx.ComboBox( self.pnl_config_general, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_strict_dlnaChoices, 0 )
		self.cmb_strict_dlna.SetToolTipString( _(u"Set this to strictly adhere to DLNA standards.\n! This will allow server-side downscaling of very large JPEG images, which may hurt JPEG serving performance on (at least) Sony DLNA products.") )
		
		fgSizer4.Add( self.cmb_strict_dlna, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText17 = wx.StaticText( self.pnl_config_general, wx.ID_ANY, _(u"Root Container"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText17.Wrap( -1 )
		fgSizer4.Add( self.m_staticText17, 0, wx.ALL, 5 )
		
		cmb_root_containerChoices = [ _(u"."), _(u"B"), _(u"M"), _(u"V"), _(u"P"), wx.EmptyString ]
		self.cmb_root_container = wx.ComboBox( self.pnl_config_general, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_root_containerChoices, 0 )
		self.cmb_root_container.SetToolTipString( _(u"Use different container as root of the tree\npossible values:\n\".\" - use standard container (this is the default)\n\"B\" - \"Browse Directory\"\n \"M\" - \"Music\"\n \"V\" - \"Video\"\n \"P\" - \"Pictures\"\n Or, you can specify the ObjectID of your desired root container (eg. 1$F for Music/Playlists)\nif you specify \"B\" and client device is audio-only then \"Music/Folders\" will be used as root") )
		
		fgSizer4.Add( self.cmb_root_container, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		bSizer8.Add( fgSizer4, 1, wx.EXPAND, 5 )
		
		
		self.pnl_config_general.SetSizer( bSizer8 )
		self.pnl_config_general.Layout()
		bSizer8.Fit( self.pnl_config_general )
		self.m_notebook3.AddPage( self.pnl_config_general, _(u"General"), False )
		self.pnl_config_network = wx.Panel( self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer7 = wx.BoxSizer( wx.HORIZONTAL )
		
		fgSizer3 = wx.FlexGridSizer( 10, 2, 0, 0 )
		fgSizer3.SetFlexibleDirection( wx.BOTH )
		fgSizer3.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText10 = wx.StaticText( self.pnl_config_network, wx.ID_ANY, _(u"Presentation URL"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText10.Wrap( -1 )
		fgSizer3.Add( self.m_staticText10, 0, wx.ALL, 5 )
		
		self.txt_presentation_url = wx.TextCtrl( self.pnl_config_network, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txt_presentation_url.SetToolTipString( _(u"Default presentation url is http address on port 80") )
		
		fgSizer3.Add( self.txt_presentation_url, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText5 = wx.StaticText( self.pnl_config_network, wx.ID_ANY, _(u"Server Name"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		fgSizer3.Add( self.m_staticText5, 0, wx.ALL, 5 )
		
		self.txt_friendly_name = wx.TextCtrl( self.pnl_config_network, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txt_friendly_name.SetToolTipString( _(u"Set this if you want to customize the name that shows up on your clients") )
		
		fgSizer3.Add( self.txt_friendly_name, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText6 = wx.StaticText( self.pnl_config_network, wx.ID_ANY, _(u"Serial"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )
		fgSizer3.Add( self.m_staticText6, 0, wx.ALL, 5 )
		
		self.txt_serial = wx.TextCtrl( self.pnl_config_network, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.txt_serial.SetToolTipString( _(u"Serial and model number the daemon will report to clients in its XML description") )
		
		fgSizer3.Add( self.txt_serial, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText7 = wx.StaticText( self.pnl_config_network, wx.ID_ANY, _(u"Model Number"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText7.Wrap( -1 )
		fgSizer3.Add( self.m_staticText7, 0, wx.ALL, 5 )
		
		self.num_model_number = wx.SpinCtrl( self.pnl_config_network, wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 1000000, 0 )
		self.num_model_number.SetToolTipString( _(u"Serial and model number the daemon will report to clients in its XML description") )
		
		fgSizer3.Add( self.num_model_number, 0, wx.ALL, 5 )
		
		
		bSizer7.Add( fgSizer3, 1, wx.EXPAND, 5 )
		
		self.m_staticline2 = wx.StaticLine( self.pnl_config_network, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL|wx.LI_VERTICAL )
		bSizer7.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )
		
		fgSizer6 = wx.FlexGridSizer( 0, 2, 0, 0 )
		fgSizer6.SetFlexibleDirection( wx.BOTH )
		fgSizer6.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.m_staticText4 = wx.StaticText( self.pnl_config_network, wx.ID_ANY, _(u"Network Interface"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText4.Wrap( -1 )
		fgSizer6.Add( self.m_staticText4, 0, wx.ALL, 5 )
		
		cmb_network_interfaceChoices = [ wx.EmptyString ]
		self.cmb_network_interface = wx.ComboBox( self.pnl_config_network, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cmb_network_interfaceChoices, 0 )
		self.cmb_network_interface.SetToolTipString( _(u"Network interfaces to bind serving. Empty means bind to all.") )
		
		fgSizer6.Add( self.cmb_network_interface, 0, wx.ALL|wx.EXPAND, 5 )
		
		self.m_staticText3 = wx.StaticText( self.pnl_config_network, wx.ID_ANY, _(u"Port"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText3.Wrap( -1 )
		fgSizer6.Add( self.m_staticText3, 0, wx.ALL, 5 )
		
		self.num_port = wx.SpinCtrl( self.pnl_config_network, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 65535, 0 )
		self.num_port.SetToolTipString( _(u"Port for HTTP (descriptions, SOAP, media transfer) traffic.Set 0 for default port") )
		
		fgSizer6.Add( self.num_port, 0, wx.ALL, 5 )
		
		self.m_staticText8 = wx.StaticText( self.pnl_config_network, wx.ID_ANY, _(u"Max  Connections"), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText8.Wrap( -1 )
		fgSizer6.Add( self.m_staticText8, 0, wx.ALL, 5 )
		
		self.num_max_connections = wx.SpinCtrl( self.pnl_config_network, wx.ID_ANY, u"0", wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 100, 0 )
		self.num_max_connections.SetToolTipString( _(u"Maximum number of simultaneous connections\nNote: many clients open several simultaneous connections while streaming") )
		
		fgSizer6.Add( self.num_max_connections, 0, wx.ALL, 5 )
		
		
		bSizer7.Add( fgSizer6, 1, wx.EXPAND, 5 )
		
		
		self.pnl_config_network.SetSizer( bSizer7 )
		self.pnl_config_network.Layout()
		bSizer7.Fit( self.pnl_config_network )
		self.m_notebook3.AddPage( self.pnl_config_network, _(u"Network"), False )
		self.pnl_config_paths = wx.Panel( self.m_notebook3, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer6 = wx.BoxSizer( wx.VERTICAL )
		
		lst_pathsChoices = []
		self.lst_paths = wx.ListBox( self.pnl_config_paths, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, lst_pathsChoices, 0 )
		self.lst_paths.SetMinSize( wx.Size( -1,380 ) )
		
		bSizer6.Add( self.lst_paths, 0, wx.ALL|wx.EXPAND, 5 )
		
		bSizer71 = wx.BoxSizer( wx.HORIZONTAL )
		
		cmb_media_dir_typeChoices = [ _(u"All"), _(u"A"), _(u"V"), _(u"P"), _(u"PV") ]
		self.cmb_media_dir_type = wx.ComboBox( self.pnl_config_paths, wx.ID_ANY, _(u"All"), wx.DefaultPosition, wx.DefaultSize, cmb_media_dir_typeChoices, 0 )
		self.cmb_media_dir_type.SetToolTipString( _(u"Set this to the directory you want scanned.\n\nIf you want to restrict a media_dir to specific content types, you\n  can set the directory type\n\"A\" for audio\n\"V\" for video \n\"P\" for images\n\"PV\" for pictures and video") )
		
		bSizer71.Add( self.cmb_media_dir_type, 0, wx.ALL, 5 )
		
		self.txt_serve_folder_select = wx.TextCtrl( self.pnl_config_paths, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer71.Add( self.txt_serve_folder_select, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.btn_serve_folder_select = wx.Button( self.pnl_config_paths, wx.ID_ANY, _(u"..."), wx.DefaultPosition, wx.Size( 30,-1 ), 0 )
		bSizer71.Add( self.btn_serve_folder_select, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.m_staticline4 = wx.StaticLine( self.pnl_config_paths, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_VERTICAL )
		bSizer71.Add( self.m_staticline4, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.btn_add_path = wx.Button( self.pnl_config_paths, wx.ID_ANY, _(u"Add"), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer71.Add( self.btn_add_path, 0, wx.ALL, 5 )
		
		self.btn_remove_path = wx.Button( self.pnl_config_paths, wx.ID_ANY, _(u"Remove"), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer71.Add( self.btn_remove_path, 0, wx.ALL, 5 )
		
		
		bSizer6.Add( bSizer71, 0, wx.EXPAND, 5 )
		
		self.lbl_wxfp_warn = wx.StaticText( self.pnl_config_paths, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl_wxfp_warn.Wrap( -1 )
		self.lbl_wxfp_warn.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_HIGHLIGHT ) )
		
		bSizer6.Add( self.lbl_wxfp_warn, 0, wx.ALL|wx.EXPAND, 5 )
		
		
		self.pnl_config_paths.SetSizer( bSizer6 )
		self.pnl_config_paths.Layout()
		bSizer6.Fit( self.pnl_config_paths )
		self.m_notebook3.AddPage( self.pnl_config_paths, _(u"Paths"), False )
		
		bSizer4.Add( self.m_notebook3, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer9 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.m_staticText151 = wx.StaticText( self.tab_config, wx.ID_ANY, _(u"If you want to disable an attribute in config file, set the value 0 or empty text."), wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText151.Wrap( -1 )
		self.m_staticText151.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_HIGHLIGHT ) )
		
		bSizer9.Add( self.m_staticText151, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.btn_save_config = wx.Button( self.tab_config, wx.ID_ANY, _(u"Save"), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer9.Add( self.btn_save_config, 0, wx.ALL|wx.ALIGN_RIGHT, 5 )
		
		
		bSizer4.Add( bSizer9, 0, wx.ALIGN_RIGHT|wx.EXPAND, 5 )
		
		
		self.tab_config.SetSizer( bSizer4 )
		self.tab_config.Layout()
		bSizer4.Fit( self.tab_config )
		self.tab_cont_main.AddPage( self.tab_config, _(u"Config"), False )
		self.tab_logs = wx.Panel( self.tab_cont_main, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer13 = wx.BoxSizer( wx.VERTICAL )
		
		self.lst_ctrl_log = wx.ListCtrl( self.tab_logs, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LC_REPORT|wx.LC_SINGLE_SEL )
		bSizer13.Add( self.lst_ctrl_log, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		self.tab_logs.SetSizer( bSizer13 )
		self.tab_logs.Layout()
		bSizer13.Fit( self.tab_logs )
		self.tab_cont_main.AddPage( self.tab_logs, _(u"Logs"), False )
		self.tab_about = wx.Panel( self.tab_cont_main, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		bSizer12 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_staticText19 = wx.StaticText( self.tab_about, wx.ID_ANY, _(u"MiniDLNA GUI\n\nVersion 0.9"), wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.m_staticText19.Wrap( -1 )
		bSizer12.Add( self.m_staticText19, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_hyperlink1 = wx.HyperlinkCtrl( self.tab_about, wx.ID_ANY, _(u"https://bitbucket.org/obayhan/minidlnagui"), u"https://bitbucket.org/obayhan/minidlnagui", wx.DefaultPosition, wx.DefaultSize, wx.HL_DEFAULT_STYLE )
		bSizer12.Add( self.m_hyperlink1, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		self.m_staticText20 = wx.StaticText( self.tab_about, wx.ID_ANY, _(u"minidlnaGUI\nGraphical frontend for minidlna.\n\nCONTRIBUTORS\nMehmet Özgür Bayhan\n\nMANY THANKS\nPython Community\nWxWidgets\nMiniDLNA\nIcon Source"), wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
		self.m_staticText20.Wrap( -1 )
		bSizer12.Add( self.m_staticText20, 0, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		
		self.tab_about.SetSizer( bSizer12 )
		self.tab_about.Layout()
		bSizer12.Fit( self.tab_about )
		self.tab_cont_main.AddPage( self.tab_about, _(u"About"), False )
		
		bSizer1.Add( self.tab_cont_main, 1, wx.EXPAND |wx.ALL, 5 )
		
		self.m_staticline1 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer1.Add( self.m_staticline1, 0, wx.EXPAND |wx.ALL, 5 )
		
		self.lbl_status = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl_status.Wrap( -1 )
		bSizer1.Add( self.lbl_status, 0, wx.ALL, 5 )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		self.timer_check = wx.Timer()
		self.timer_check.SetOwner( self, wx.ID_ANY )
		self.timer_check.Start( 1000 )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.event_form_close )
		self.tab_cont_main.Bind( wx.EVT_NOTEBOOK_PAGE_CHANGED, self.event_tab_cont_main_changed )
		self.btn_Start.Bind( wx.EVT_BUTTON, self.event_btn_Start_clicked )
		self.btn_Stop.Bind( wx.EVT_BUTTON, self.event_btn_Stop_clicked )
		self.btn_Rebuild.Bind( wx.EVT_BUTTON, self.event_btn_Rebuild_clicked )
		self.btn_minidlna_path_select.Bind( wx.EVT_BUTTON, self.event_btn_minidlna_path_select_click )
		self.btn_serve_folder_select.Bind( wx.EVT_BUTTON, self.event_btn_serve_folder_select_click )
		self.btn_add_path.Bind( wx.EVT_BUTTON, self.event_btn_add_path_click )
		self.btn_remove_path.Bind( wx.EVT_BUTTON, self.event_btn_remove_path_click )
		self.btn_save_config.Bind( wx.EVT_BUTTON, self.event_btn_save_config_clicked )
		self.Bind( wx.EVT_TIMER, self.event_timer_check, id=wx.ID_ANY )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def event_form_close( self, event ):
		event.Skip()
	
	def event_tab_cont_main_changed( self, event ):
		event.Skip()
	
	def event_btn_Start_clicked( self, event ):
		event.Skip()
	
	def event_btn_Stop_clicked( self, event ):
		event.Skip()
	
	def event_btn_Rebuild_clicked( self, event ):
		event.Skip()
	
	def event_btn_minidlna_path_select_click( self, event ):
		event.Skip()
	
	def event_btn_serve_folder_select_click( self, event ):
		event.Skip()
	
	def event_btn_add_path_click( self, event ):
		event.Skip()
	
	def event_btn_remove_path_click( self, event ):
		event.Skip()
	
	def event_btn_save_config_clicked( self, event ):
		event.Skip()
	
	def event_timer_check( self, event ):
		event.Skip()
	

