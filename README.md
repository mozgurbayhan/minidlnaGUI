# minidlnaGUI #
Graphical frontend for minidlna. 

## CONTRIBUTORS ##

* Mehmet Özgür Bayhan ( mozgurbayhan@gmail.com )

## License ##
* [BSD License](https://opensource.org/licenses/BSD-3-Clause)

## MANY THANKS ##

* [Python2 Community](https://www.python.org/)
* [WxWidgets2](https://wxwidgets.org/)
* [MiniDLNA](http://sourceforge.net/projects/minidlna/)
* Icon -> [Madebyoliver](http://www.flaticon.com/authors/madebyoliver) -> [License](http://file005.flaticon.com/downloads/license/license.pdf)